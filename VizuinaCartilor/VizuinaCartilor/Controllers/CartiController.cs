﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AccesDateCarti;

namespace VizuinaCartilor.Controllers
{
    public class CartiController : ApiController
    {
        [HttpGet]
        public IEnumerable<Carti> LoadAllBook()
        {
            //incarcarea paginii cu toate cartile din bd

            using (CartileDBEntities entities = new CartileDBEntities())
            {
                return entities.Carti.ToList();
            }
        }
        [HttpGet]
        public HttpResponseMessage LoadBookById(int id)
        {
            //incarcarea paginii cu cartea care are id-ul dat

            using (CartileDBEntities entities = new CartileDBEntities())
            {
                var entity = entities.Carti.FirstOrDefault(e => e.ID == id);
                if (entity != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                        "Book with Id " + id.ToString() + " not found");
                }
            }
        }

        public HttpResponseMessage Post([FromBody] Carti carti)
        {
            try
            {
                using (CartileDBEntities entities = new CartileDBEntities())
                {
                    entities.Carti.Add(carti);
                    entities.SaveChanges();
                    var message = Request.CreateResponse(HttpStatusCode.Created, carti);
                    message.Headers.Location = new Uri(Request.RequestUri + "/" +carti.ID.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            try
            {
                using (CartileDBEntities entities = new CartileDBEntities())
                {
                    var entity = entities.Carti.FirstOrDefault(e => e.ID == id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,"Book with Id = " + id.ToString() + " not found to delete");
                    }
                    else
                    {
                        entities.Carti.Remove(entity);
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, [FromBody]Carti carte)
        {
            try
            {
                using (CartileDBEntities entities = new CartileDBEntities())
                {
                    var entity = entities.Carti.FirstOrDefault(e => e.ID == id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,"Book with Id " + id.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.Nume = carte.Nume;
                        entity.Autor = carte.Autor;
                        entity.Descriere = carte.Descriere;
                        entity.Pret = carte.Pret;
                        entity.Cantitatea = carte.Cantitatea;

                        entities.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}